using System;
namespace Seongnam.BAL 
{
    public class Person
    {
        public Guid ID { get; set; } = Guid.NewGuid();
        public string LegalName { get; set; }
        public string PreferredName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime? DateOfDeath { get; set; }
        public Person(string legalName, string preferredName, DateTime dateOfBirth, DateTime? dateOfDeath = null)
        {
            this.LegalName = legalName;
            this.PreferredName = preferredName;
            this.DateOfBirth = dateOfBirth;
            this.DateOfDeath = dateOfDeath;
        }
        public int getAgeInYears(DateTime currentDate)
        {
            if (DateOfDeath != null && DateOfDeath - DateOfBirth > currentDate - DateOfBirth)
            {
                var nullableTimespan = (DateOfDeath - DateOfBirth);
                if (nullableTimespan.HasValue) 
                {
                    return (int)Math.Floor((currentDate - DateOfBirth).TotalDays / 365);
                }
                return -1;
            }
            else 
            {
                return (int)Math.Floor((currentDate - DateOfBirth).TotalDays / 365);
            }
        }
    }
}