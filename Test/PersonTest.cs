using System;
using Xunit;
using Seongnam.BAL;
namespace Test
{
    public class PersonTest
    {
        [Fact]
        public void PersonShouldCreate()
        {
            Person person = new Person(
                legalName: "Adam Smith, FRSA", 
                preferredName: "Adam Babu", 
                dateOfBirth: new DateTime(year: 1723, month: 6, day: 16), 
                dateOfDeath: new DateTime(year: 1790, month: 7, day: 17));
        }
    }
}
